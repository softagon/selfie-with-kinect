﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Threading;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Facebook;

namespace KinectPhoto
{
    class FacebookUploader
    {
        public const string SHARED_MESSAGE = "This picture was shared on facebook.com/";
        private Dictionary<string, string> uploadedPhotos;

        private static bool IsFacebookConfigurated = false;

        public FacebookUploader()
        {
            uploadedPhotos = new Dictionary<string, string>();
            VerifyFacebookConfiguration();
            try
            {
                GetAccessToken();
            }
            catch (FacebookOAuthException) { }
            catch (Exception) { }

            if (!IsFacebookConfigurated) MessageBox.Show("Configure o facebook corretamente para postar fotos");
        }

        private static void VerifyFacebookConfiguration()
        {
            try
            {
                if (!string.IsNullOrEmpty(Settings.Get(Settings.FbAppToken)))
                {
                    FacebookClient fbClient = new FacebookClient(Settings.Get(Settings.FbAppToken));
                    dynamic result = fbClient.Get("debug_token", new { input_token = Settings.Get(Settings.FbLongToken) });
                    var appId = result.data.app_id;
                    var isValid = result.data.is_valid;
                    var application = result.data.application;
                    var userId = result.data.user_id;
                    var expiresAt = result.data.expires_at;
                    var scopes = result.data.scopes;

                    if (isValid) IsFacebookConfigurated = true;
                    else IsFacebookConfigurated = false;
                }
                else if (!string.IsNullOrEmpty(Settings.Get(Settings.FbLongToken)))
                {
                    FacebookClient fbClient = new FacebookClient(Settings.Get(Settings.FbLongToken));
                    IsFacebookConfigurated = true;
                }
            }
            catch (FacebookOAuthException)
            { IsFacebookConfigurated = false; }
            catch (Exception)
            { IsFacebookConfigurated = false; }
        }

        private void GetAccessToken()
        {
            var fb = new FacebookClient();
            try
            {
                fb = new FacebookClient(Settings.Get(Settings.FbAppToken));
                //dynamic accounts = fb.Get("/" + Settings.GetInstance().Get("fbUserID") + "/accounts");
                dynamic accounts = fb.Get("/me/accounts");
                foreach (dynamic account in accounts.data)
                {
                    if (account.id == Settings.Get(Settings.FbPageID))
                    {
                        Settings.Set(Settings.FbLongToken, account.access_token);
                        break;
                    }
                }
            }
            catch (FacebookOAuthException e)
            {
                IsFacebookConfigurated = false;
                throw new FacebookOAuthException("Configure novamente o AppToken, pois o mesmo encontra-se invalido.", e);
                
            }
            catch (Exception)
            {
                IsFacebookConfigurated = false;
                throw new Exception("O token não possui permissão para requisitar as suas paginas");
            }

        }


        public void ThreadRun()
        {
            while (true)
            {
                if (IsFacebookConfigurated)
                {
                    UploadPhotos();
                    Thread.Sleep(TimeSpan.FromSeconds(10));
                }
            }
        }

        private void UploadPhotos()
        {
            string[] files = Directory.GetFiles(Environment.CurrentDirectory + Directories.DIR_NO_UPLOADED_PHOTOS);
            foreach (string file in files)
            {
                MemoryStream mStream = new MemoryStream();
                try
                {
                    FileStream fStream = new FileStream(file, FileMode.Open);
                    fStream.CopyTo(mStream);
                    fStream.Dispose();

                    byte[] img = mStream.ToArray();
                    if (SharePhoto(img))
                    {
                        string pathFile = Environment.CurrentDirectory + Directories.DIR_UPLOADED_PHOTOS + file.Substring(Environment.CurrentDirectory.Length + Directories.DIR_NO_UPLOADED_PHOTOS.Length);
                        if (!File.Exists(pathFile))
                            File.Move(file, pathFile);
                        else
                            File.Delete(file);
                    }
                    else
                        break;
                }
                catch (UnauthorizedAccessException e) { MessageBox.Show(e.Message, "Você precisa de permissão para acessar este arquivo."); }
                catch (PathTooLongException e) { MessageBox.Show(e.Message, "Caminho muito longo"); }
                catch (DirectoryNotFoundException e) { MessageBox.Show(e.Message, "Diretório não encontrado."); }
                catch (Exception e) { MessageBox.Show(e.Message); }
                finally
                {
                    mStream.Dispose();
                }
            }
        }

        private bool SharePhoto(byte[] byteImage)
        {
            try
            {
                FacebookClient fbClient = new FacebookClient(Settings.Get(Settings.FbLongToken));

                var media = new FacebookMediaObject
                {
                    ContentType = "image/jpeg",
                    FileName = "selfie"
                }.SetValue(byteImage);

                var parameters = new Dictionary<string, object>();
                parameters["message"] = Settings.Get(Settings.FbMessage);
                parameters["caption"] = string.Empty;
                parameters["description"] = string.Empty;
                parameters["name"] = Settings.Get(Settings.FbMessage);
                parameters["req_perms"] = "publish_stream";
                parameters["scope"] = "publish_stream";
                parameters["source"] = media;
                parameters["type"] = "normal";

                fbClient.PostTaskAsync("/" + Settings.Get(Settings.FbPageID) + "/photos", parameters);

                //GetAccessToken();

                return true;
            }
            catch (FacebookOAuthException) {}

            return false;
        }
    }
}
