﻿using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;

namespace KinectPhoto.Helper
{
    class Utils
    {

        public static ImageSource GetImage(string parentPath, string fileName)
        {
            var uri = new Uri(parentPath+fileName, UriKind.Absolute);
            return new BitmapImage(uri);
        }

        public static bool HasSomeone(short[] depthImage)
        {
            foreach (short depthPixel in depthImage)
            {
                int player = depthPixel & DepthImageFrame.PlayerIndexBitmask;
                if (player > 0)//existe alguém
                {
                    return true;
                }
            }
            return false;
        }

        public static bool Wave(Skeleton skeleton)
        {
            if (skeleton.Joints[JointType.HandRight].Position.Y < skeleton.Joints[JointType.Head].Position.Y &&
                skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.ElbowRight].Position.Y &&
                skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.WristRight].Position.Y &&
                skeleton.Joints[JointType.HandRight].Position.Y > skeleton.Joints[JointType.HandLeft].Position.Y)
            {
                return true;
            }
            return false;
        }
    }
}
