﻿using System;
using System.Collections.Generic;
using Microsoft.Kinect;

namespace KinectPhoto
{
    interface IRecognizerGestures
    {
        string Name { get; }
        bool Check(Skeleton s);
    }
}
