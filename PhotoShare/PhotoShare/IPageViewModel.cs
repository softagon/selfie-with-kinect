﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectPhoto
{
    public interface IPageViewModel
    {
        string NamePage { get; }

        void Show();
        void Hide();
    }
}
