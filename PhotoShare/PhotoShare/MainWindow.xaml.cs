﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Net;
using System.Xml;
using System.IO;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect.Toolkit.FaceTracking;
using Facebook;
using KinectPhoto.Configuration;
using KinectPhoto.Helper;

namespace KinectPhoto
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private KinectSensorChooser sensorChooser = new KinectSensorChooser();

        private WriteableBitmap colorBitmap;
        private byte[] colorPixel;

        private short[] depthImage;
        private Skeleton[] skeletonData;

        private bool isTookPhoto = false;
        private bool isTakingPhoto = false;

        /*
         * To wave gesture recognition
         */
        private int pauseFrameCount = 10;
        private int frameCount = 0;
        private bool isPaused;
        private IRecognizerGestures[] waveGesture;
        private int currentGesture = 0;

        private DispatcherTimer nobodyDispatcher;
        private DispatcherTimer countdownDispatcher;
        private int time = 3;

        private FacebookUploader fbUploader;
        private Thread uploadThread;

        public static string _iconChanged = "";

        private ConfigurationWindow configWindow;
        private FaceTracker faceTraker;

        public MainWindow()
        {
            InitializeComponent();
            SettingConfiguration();

            this.sensorChooser.KinectChanged += sensor_KinectChanged;
            this.sensorUI.KinectSensorChooser = this.sensorChooser;
            this.sensorChooser.Start();
        }

        private void SettingConfiguration()
        {
            string path = Settings.Get(Settings.Video);
            if (File.Exists(path))
            {
                VideoTutorial.Source = new Uri(path, UriKind.Absolute);
                VideoTutorial.Play();
            }
            else
            {
                PhotoRoom.Visibility = System.Windows.Visibility.Visible;
                VideoRoom.Visibility = System.Windows.Visibility.Hidden;
            }

            fbUploader = new FacebookUploader();
            uploadThread = new Thread(new ThreadStart(fbUploader.ThreadRun));
            uploadThread.Name = "FacebookUploader";
            uploadThread.IsBackground = true;
            try
            {
                if (!uploadThread.IsAlive)
                    uploadThread.Start();
            }
            catch (ThreadStartException) { }
            catch (ThreadStateException) { }
            catch (ThreadInterruptedException) { }

            string parentIconPath = Environment.CurrentDirectory + Directories.DIR_ICONS;
            this.iconConfigButton.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconConfig));


            countdownDispatcher = new DispatcherTimer();
            countdownDispatcher.Interval = new TimeSpan(0, 0, 1);
            countdownDispatcher.Tick += dispatcherTimer_Tick;

            nobodyDispatcher = new DispatcherTimer();
            nobodyDispatcher.Interval = new TimeSpan(0, 2, 0);
            nobodyDispatcher.Tick += nobodyDispatcher_Tick;

            waveGesture = new IRecognizerGestures[4];
            waveGesture[0] = new RightWave();
            waveGesture[1] = new LeftWave();
            waveGesture[2] = new RightWave();
            waveGesture[3] = new LeftWave();

            try { ChangeButtonProperty(); }
            catch (XmlException) { }
        }

        public void ChangeButtonProperty()
        {
            string parentIconPath = Environment.CurrentDirectory + Directories.DIR_ICONS;
            this.IconPhoto.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconTake));
            this.IconRephoto.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconRetake));
            this.IconShare.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconShare));
            this.IconPrint.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconPrint));
            this.iconConfigButton.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconConfig));
            parentIconPath = Environment.CurrentDirectory + Directories.DIR_LOGO;
            this.Logo.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.Logo));

            this.Logo.Height = int.Parse(Settings.Get(Settings.LogoHeight));
            this.Logo.Width = int.Parse(Settings.Get(Settings.LogoWidth));
        }

        private void ChangeIcon()
        {
            string parentIconPath = Environment.CurrentDirectory + Directories.DIR_ICONS;
            switch (_iconChanged)
            {
                case Settings.IconConfig:
                    this.iconConfigButton.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconConfig));
                    break;
                case Settings.IconPrint:
                    this.IconPrint.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconPrint));
                    break;
                case Settings.IconRetake:
                    this.IconRephoto.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconRetake));
                    break;
                case Settings.IconShare:
                    this.IconShare.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconShare));
                    break;
                case Settings.IconTake:
                    this.IconPhoto.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconTake));
                    break;
                case Settings.Logo:
                    parentIconPath = Environment.CurrentDirectory + Directories.DIR_LOGO;
                    this.Logo.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.Logo));
                    break;
            }
            _iconChanged = "";
        }

        private void sensor_KinectChanged(object sender, KinectChangedEventArgs e)
        {
            bool error = false;
            /*if (e.OldSensor != null)
            {
                e.OldSensor.Stop();
            }*/

            if (e.NewSensor != null)
            {
                try
                {
                    e.NewSensor.ColorStream.Enable(ColorImageFormat.RgbResolution1280x960Fps12);
                    e.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);

                    e.NewSensor.DepthStream.Range = DepthRange.Default;
                    e.NewSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    e.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;

                    e.NewSensor.SkeletonStream.Enable();

                    this.colorPixel = new byte[e.NewSensor.ColorStream.FramePixelDataLength];

                    this.colorBitmap = new WriteableBitmap(e.NewSensor.ColorStream.FrameWidth, e.NewSensor.ColorStream.FrameHeight, 96.0, 96.0, PixelFormats.Bgr32, null);
                    this.ImageCam.Source = this.colorBitmap;

                    e.NewSensor.ElevationAngle = 0;
                    e.NewSensor.AllFramesReady += AllFramesReady;
                }
                catch (InvalidOperationException)
                {
                    error = true;
                }
            }

            if (!error)
                this.kinectRegion.KinectSensor = e.NewSensor;
        }

        void AllFramesReady(object sender, AllFramesReadyEventArgs allFramesArgs)
        {
            if (!string.IsNullOrEmpty(_iconChanged))
            {
                ChangeIcon();
            }

            if (kinectRegion.KinectSensor == null || configWindow != null) return;

            ColorImageFrame colorImageFrame = allFramesArgs.OpenColorImageFrame(); ;
            DepthImageFrame depthImageFrame = allFramesArgs.OpenDepthImageFrame(); ;
            SkeletonFrame skeletonFrame = allFramesArgs.OpenSkeletonFrame(); ;

            try
            {
                /*colorImageFrame = allFramesArgs.OpenColorImageFrame();
                depthImageFrame = allFramesArgs.OpenDepthImageFrame();
                skeletonFrame = allFramesArgs.OpenSkeletonFrame();*/

                if (colorImageFrame == null || depthImageFrame == null || skeletonFrame == null)
                {
                    return;
                }

                // Check for image format changes. The FaceTracker doesn't
                // deal with that so we need to reset.
                /*if (this.depthImageFormat != depthImageFrame.Format)
                {
                    this.ResetFaceTracking();
                    this.depthImage = null;
                    this.depthImageFormat = depthImageFrame.Format;
                }

                if (this.colorImageFormat != colorImageFrame.Format)
                {
                    this.ResetFaceTracking();
                    this.colorImage = null;
                    this.colorImageFormat = colorImageFrame.Format;
                }*/

                // Create any buffers to store copies of the data we work with
                if (this.depthImage == null)
                {
                    this.depthImage = new short[depthImageFrame.PixelDataLength];
                }

                if (this.colorPixel == null)
                {
                    this.colorPixel = new byte[colorImageFrame.PixelDataLength];
                }

                // Get the skeleton information
                if (this.skeletonData == null || this.skeletonData.Length != skeletonFrame.SkeletonArrayLength)
                {
                    this.skeletonData = new Skeleton[skeletonFrame.SkeletonArrayLength];
                }

                colorImageFrame.CopyPixelDataTo(this.colorPixel);
                depthImageFrame.CopyPixelDataTo(this.depthImage);
                skeletonFrame.CopySkeletonDataTo(this.skeletonData);

                #region VideoRoom Methods
                if (VideoRoom.Visibility == System.Windows.Visibility.Visible)
                {
                    Wave(skeletonData);
                }
                #endregion

                #region PhotoRoom Methods
                if (PhotoRoom.Visibility != System.Windows.Visibility.Visible) return;

                if (File.Exists(Settings.Get(Settings.Video)))
                {

                    if (Utils.HasSomeone(this.depthImage))
                    {
                        nobodyDispatcher.Stop();
                    }
                    else
                    {
                        if (!nobodyDispatcher.IsEnabled)
                            nobodyDispatcher.Start();
                    }
                }

                if (isTookPhoto) return;

                this.colorBitmap.WritePixels(new Int32Rect(0, 0, this.colorBitmap.PixelWidth, this.colorBitmap.PixelHeight),
                    this.colorPixel, this.colorBitmap.PixelWidth * sizeof(int), 0);

                if (isTakingPhoto)
                    SmileText.Text = HasSmile(colorImageFrame, depthImageFrame, this.skeletonData) ? Settings.Get(Settings.SmileText) : Settings.Get(Settings.NoSmileText);

                #endregion
            }
            catch (NullReferenceException) { }
            catch (Exception) { }
            finally
            {
                if (colorImageFrame != null) colorImageFrame.Dispose();
                if (depthImageFrame != null) depthImageFrame.Dispose();
                if (skeletonFrame != null) skeletonFrame.Dispose();
            }
        }

        private void Wave(Skeleton[] skeletonData)
        {
            foreach (Skeleton skeleton in skeletonData)
            {
                if (isPaused)
                {
                    if (pauseFrameCount == frameCount)
                    {
                        frameCount = 0;
                        isPaused = false;
                    }
                    frameCount++;
                }

                bool result = waveGesture[currentGesture].Check(skeleton);
                if (result)
                {
                    if (currentGesture == waveGesture.Length - 1)
                    {
                        PhotoRoom.Visibility = System.Windows.Visibility.Visible;
                        VideoRoom.Visibility = System.Windows.Visibility.Hidden;
                        VideoTutorial.Stop();
                        ResetWaveGesture();
                        return;
                    }
                    currentGesture++;
                    frameCount = 0;
                    pauseFrameCount = 10;
                    isPaused = true;
                }
                else if (frameCount >= 50)
                {
                    ResetWaveGesture();
                }
                else
                {
                    frameCount++;
                    pauseFrameCount = 5;
                    isPaused = true;
                }

            }
        }
        
        public void ResetWaveGesture()
        {
            this.currentGesture = 0;
            this.frameCount = 0;
            this.pauseFrameCount = 5;
            this.isPaused = true;
        }

        private bool HasSmile(ColorImageFrame colorImageFrame, DepthImageFrame depthImageFrame, Skeleton[] skeletonData)
        {
            // Update the list of trackers and the trackers with the current frame information
            foreach (Skeleton skeleton in this.skeletonData)
            {
                if (skeleton.TrackingState == SkeletonTrackingState.Tracked || skeleton.TrackingState == SkeletonTrackingState.PositionOnly)
                {
                    if (faceTraker == null)
                        faceTraker = new FaceTracker(sensorChooser.Kinect);

                    FaceTrackFrame frame = faceTraker.Track(colorImageFrame.Format, this.colorPixel, depthImageFrame.Format, this.depthImage, skeleton);

                    try
                    {
                        float lipCornerDepress = frame.GetAnimationUnitCoefficients()[AnimationUnit.LipCornerDepressor];
                        if (lipCornerDepress < double.Parse(Settings.Get(Settings.LipCornerDepress)))//-0.2)
                            return true;
                        else
                        {
                            float lipStretcher = frame.GetAnimationUnitCoefficients()[AnimationUnit.LipStretcher];
                            if (lipStretcher > double.Parse(Settings.Get(Settings.LipStretch)))// 0.6f)
                                return true;

                            return false;
                        }
                    }
                    catch (Exception) { }
                }
            }
            return false;
        }

        void nobodyDispatcher_Tick(object sender, EventArgs e)
        {
            PhotoRoom.Visibility = System.Windows.Visibility.Hidden;
            VideoRoom.Visibility = System.Windows.Visibility.Visible;
            if (VideoTutorial.Source == null)
                VideoTutorial.Source = new Uri(Settings.Get(Settings.Video));
            VideoTutorial.Play();
        }

        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (this.sensorChooser != null)
            {
                if (this.sensorChooser.Kinect != null)
                {
                    this.sensorChooser.Kinect.DepthStream.Disable();
                    this.sensorChooser.Kinect.ColorStream.Disable();
                    this.sensorChooser.Kinect.SkeletonStream.Disable();
                    this.sensorChooser.Kinect.Stop();
                }
                this.sensorChooser.Stop();

                this.sensorChooser = null;
            }

            if (uploadThread != null && uploadThread.IsAlive)
            {
                try { uploadThread.Abort(); }
                catch (ThreadAbortException) { }
            }
            if (faceTraker != null)
                faceTraker.Dispose();
            App.Current.Shutdown();
        }

        private void TakePhoto_Click(object sender, RoutedEventArgs e)
        {
            if (this.kinectRegion.KinectSensor == null)
            {
                MessageBox.Show("Verifique se o Kinect está corretamente conectado.");
                return;
            }

            PhotoButton.Visibility = System.Windows.Visibility.Hidden;
            isTakingPhoto = true;
            try
            {
                CountdownText.Foreground = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(Settings.Get(Settings.CountColor))));
                CountdownText.FontFamily = new System.Windows.Media.FontFamily(Settings.Get(Settings.CountFontFamily));
                CountdownText.FontSize = double.Parse(Settings.Get(Settings.CountFontSize));

                SmileText.Foreground = new SolidColorBrush((Color)(ColorConverter.ConvertFromString(Settings.Get(Settings.SmileColor))));
                SmileText.FontFamily = new System.Windows.Media.FontFamily(Settings.Get(Settings.SmileFontFamily));
                SmileText.FontSize = double.Parse(Settings.Get(Settings.SmileFontSize));
            }
            catch (InvalidCastException) { }
            catch (FormatException) { }
            catch (Exception) { }

            CountdownText.Visibility = System.Windows.Visibility.Visible;
            SmileText.Visibility = System.Windows.Visibility.Visible;

            countdownDispatcher.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            if (time > 0)
            {
                CountdownText.Text = time.ToString();
                time--;
            }
            else
            {
                countdownDispatcher.Stop();
                CountdownText.Text = "";
                SmileText.Text = "";
                time = 3;
                TakePhoto();
                CountdownText.Visibility = System.Windows.Visibility.Hidden;
                SmileText.Visibility = System.Windows.Visibility.Hidden;
            }

        }

        private void TakePhoto()
        {
            int colorWidth = this.colorBitmap.PixelWidth;
            int colorHeight = this.colorBitmap.PixelHeight;

            var renderBitmap = new RenderTargetBitmap(colorWidth, colorHeight, 96.0, 96.0, PixelFormats.Pbgra32);

            var dv = new DrawingVisual();
            using (var dc = dv.RenderOpen())
            {
                var colorBrush = new VisualBrush(this.ImageCam);
                dc.DrawRectangle(colorBrush, null, new System.Windows.Rect(new System.Windows.Point(), new Size(colorWidth, colorHeight)));

                var logoBrush = new VisualBrush(this.Logo);
                dc.DrawRectangle(logoBrush, null, new System.Windows.Rect(new System.Windows.Point(), new Size(Logo.Width, Logo.Height)));
            }

            renderBitmap.Render(dv);

            BitmapEncoder encoder = new PngBitmapEncoder();

            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));

            this.ImageCam.Source = encoder.Frames[0];

            ShareButton.Visibility = System.Windows.Visibility.Visible;
            RePhotoButton.Visibility = System.Windows.Visibility.Visible;
            PrintButton.Visibility = System.Windows.Visibility.Visible;
            Logo.Visibility = System.Windows.Visibility.Hidden;

            isTookPhoto = true;
            isTakingPhoto = false;
        }

        private void RetakePhoto_Click(object sender, RoutedEventArgs e)
        {
            RetakePhoto();
        }

        private void BackPhotoGrid(object sender, RoutedEventArgs e)
        {
            PhotoGrid.Visibility = System.Windows.Visibility.Visible;
            SharedGrid.Visibility = System.Windows.Visibility.Hidden;

            RetakePhoto();
        }

        private void RetakePhoto()
        {
            if (this.kinectRegion.KinectSensor == null) return;

            ShareButton.Visibility = System.Windows.Visibility.Hidden;
            RePhotoButton.Visibility = System.Windows.Visibility.Hidden;
            PrintButton.Visibility = System.Windows.Visibility.Hidden;

            PhotoButton.Visibility = System.Windows.Visibility.Visible;
            this.Logo.Visibility = System.Windows.Visibility.Visible;

            this.ImageCam.Source = this.colorBitmap;

            isTookPhoto = false;
        }

        private void SavePhoto_Click(object sender, RoutedEventArgs args)
        {
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(this.ImageCam.Source as BitmapSource));

            JpegBitmapEncoder jpgEncoder = new JpegBitmapEncoder();
            jpgEncoder.Frames.Add(encoder.Frames[0]);

            string name = "Selfie_" + DateTime.Now.ToString("dd'-'MM'-'yyyy'-'hh'-'mm'-'ss", System.Globalization.CultureInfo.CurrentUICulture.DateTimeFormat) + ".jpg";
            string path = Environment.CurrentDirectory + Directories.DIR_NO_UPLOADED_PHOTOS + name;
            try
            {
                using (FileStream file = new FileStream(path, FileMode.Create))
                {
                    encoder.Save(file);
                    file.Dispose();
                }

                this.PhotoGrid.Visibility = System.Windows.Visibility.Hidden;
                this.SharedGrid.Visibility = System.Windows.Visibility.Visible;

                this.SharedImage.Source = this.ImageCam.Source;
                this.SharedText.Text = FacebookUploader.SHARED_MESSAGE + Settings.Get(Settings.FbPageName);
            }
            catch (IOException e)
            { MessageBox.Show(e.Message); }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();
            bool? result = printDialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                StackPanel myImg = new StackPanel();

                myImg.Children.Add(this.ImageCam);

                myImg.Measure(new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight));
                myImg.Arrange(new System.Windows.Rect(new System.Windows.Point(0, 0), myImg.DesiredSize));


                printDialog.PrintVisual(myImg, "selfie");
            }
        }

        private void Config_Click(object sender, RoutedEventArgs e)
        {
            if (configWindow != null) return;

            configWindow = new ConfigurationWindow();
            if (configWindow.ShowDialog() == false)
            {
                configWindow.Close();
                configWindow = null;
            }
        }

        private void VideoTutorial_MediaEnded(object sender, RoutedEventArgs e)
        {
            VideoTutorial.Position = TimeSpan.Zero;
            VideoTutorial.Play();
        }
    }
}
