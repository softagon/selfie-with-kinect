﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using KinectPhoto.Helper;

namespace KinectPhoto.Configuration
{
    /// <summary>
    /// Interaction logic for ConfigurationWindow.xaml
    /// </summary>
    public partial class ConfigurationWindow : Window
    {

        public List<IPageViewModel> pages = new List<IPageViewModel>();

        public ConfigurationWindow()
        {
            InitializeComponent();

            pages.Add(new GeneralConfiguration());
            pages.Add(new IconsConfiguration());
            pages.Add(new TextConfiguration());
            pages.Add(new FacebookConfiguration());
            pages.Add(new ImageGalery(Directories.DIR_UPLOADED_PHOTOS));
            pages.Add(new ImageGalery(Directories.DIR_NO_UPLOADED_PHOTOS));

            TextBlock initialMessage = new TextBlock();
            initialMessage.Text = "Navegue ao lado nas possiveis configurações";
            initialMessage.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            initialMessage.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            initialMessage.FontSize = 26;
            CurrentPage.Content = initialMessage;
        }

        public object CurrentPageView
        {
            get
            {
                return CurrentPage.Content;
            }
        }

        private void GeneralConfig_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentPage.Content.GetType() != typeof(GeneralConfiguration))
            {
                CurrentPage.Content = pages.First(p => p is GeneralConfiguration);
                (CurrentPage.Content as IPageViewModel).Show();
            }
        }

        private void Images_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentPageView.GetType() != typeof(IconsConfiguration))
            {
                //CurrentPageView = new IconsConfiguration();
                CurrentPage.Content = pages.FirstOrDefault(p => p is IconsConfiguration);
                (CurrentPage.Content as IPageViewModel).Show();
            }
        }

        private void Texto_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentPageView.GetType() != typeof(TextConfiguration))
            {
                CurrentPage.Content = pages.FirstOrDefault(p => p is TextConfiguration);
                (CurrentPage.Content as IPageViewModel).Show();
            }
        }

        private void Facebook_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentPageView.GetType() != typeof(FacebookConfiguration))
            {
                CurrentPage.Content = pages.FirstOrDefault(p => p is FacebookConfiguration);
                (CurrentPage.Content as IPageViewModel).Show();
            }
        }

        private void Uploaded_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentPageView.GetType() != typeof(ImageGalery) ||
                //(CurrentPageView.GetType() == typeof(ImageGalery) && (CurrentPageView as ImageGalery).Name != "UPLOADED"))
                (CurrentPageView.GetType() == typeof(ImageGalery) && !(CurrentPageView as ImageGalery).Name.Contains(Directories.DIR_UPLOADED_PHOTOS)))
            {
                //CurrentPage.Content = new ImageGalery("UPLOADED");
                CurrentPage.Content = pages.FirstOrDefault(p => p is ImageGalery && p.NamePage.Contains(Directories.DIR_UPLOADED_PHOTOS));
                (CurrentPage.Content as IPageViewModel).Show();
            }
        }

        private void NoUploaded_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentPageView.GetType() != typeof(ImageGalery) ||
                (CurrentPageView.GetType() == typeof(ImageGalery) && !(CurrentPageView as ImageGalery).Name.Contains(Directories.DIR_NO_UPLOADED_PHOTOS)))
            {
                //CurrentPage.Content = new ImageGalery("NO_UPLOADED");
                CurrentPage.Content = pages.FirstOrDefault(p => p is ImageGalery && p.NamePage.Contains(Directories.DIR_NO_UPLOADED_PHOTOS) );
                (CurrentPage.Content as IPageViewModel).Show();
            }
        }
    }
}
