﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Facebook;

namespace KinectPhoto.Configuration
{
    /// <summary>
    /// Interaction logic for FacebookConfiguration.xaml
    /// </summary>
    public partial class FacebookConfiguration : UserControl, IPageViewModel
    {
        public string NamePage { get { return "Icones"; } }
        public FacebookConfiguration()
        {
            InitializeComponent();
        }

        public void Show()
        {
            pageName.Text = Settings.Get(Settings.FbPageName);
            pageID.Text = Settings.Get(Settings.FbPageID);
            pageToken.Text = Settings.Get(Settings.FbLongToken);
            appSecret.Text = Settings.Get(Settings.FbAppSecret);
            appToken.Text = Settings.Get(Settings.FbAppToken);
            message.Text = Settings.Get(Settings.FbMessage);
        }

        public void Hide()
        {
        }

        public void Save_Click(object sender, RoutedEventArgs e)
        {
            Settings.Set(Settings.FbPageName, pageName.Text);
            Settings.Set(Settings.FbPageID, pageID.Text);
            Settings.Set(Settings.FbLongToken, pageToken.Text);
            Settings.Set(Settings.FbAppToken, appToken.Text);
            Settings.Set(Settings.FbAppSecret, appSecret.Text);
            Settings.Set(Settings.FbMessage, message.Text);
        }

        private void ExtendToken_Click()
        {
            try
            {
                var fb = new FacebookClient();
                dynamic result = fb.Get("oauth/access_token", new
                {
                    client_id = pageID.Text,
                    client_secret = appSecret.Text,
                    grant_type = "fb_exchange_token",
                    fb_exchange_token = pageToken.Text
                });

                if (result.error != null)
                    MessageBox.Show("Não foi possível extender o token.");
                else if (result.acess_token != null)
                {
                    MessageBox.Show("Token extendido e atualizado");
                    Settings.Set(Settings.FbLongToken, result.acess_token);
                    pageToken.Text = Settings.Get(Settings.FbLongToken);
                }
            }
            catch (Exception)
            { }
        }

        public void Debug_Click(object sender, RoutedEventArgs args)
        {
            try
            {
                if (!string.IsNullOrEmpty(appToken.Text))
                {
                    FacebookClient fbClient = new FacebookClient(appToken.Text);
                    dynamic result = fbClient.Get("debug_token", new { input_token = pageToken.Text });
                    var appId = result.data.app_id;
                    var isValid = result.data.is_valid;
                    var application = result.data.application;
                    var userId = result.data.user_id;
                    var expiresAt = result.data.expires_at;
                    var scopes = result.data.scopes;

                    if (isValid)
                    {
                        //MessageBox.Show("Page token válido!\n" + (expiresAt.ToString() == "0" || expiresAt.ToString() == "Never") ? "O page token não irá expirar" : "O page token expira em:" + (new DateTime(expiresAt).Subtract(DateTime.Now)).Days + "dia(s)", "Configuração");
                        //MessageBox.Show("Page token válido!\nO Page Token expira em: " + expiresAt + "dia(s)", "Configuração");
                        MessageBox.Show("Page Token válido!", "Configuração");
                        ExtendToken_Click();
                    }
                    else
                        MessageBox.Show("Page token inválido!\nGere outro Page token.");
                }
                else if (!string.IsNullOrEmpty(pageToken.Text))
                {
                    FacebookClient fbClient = new FacebookClient(pageToken.Text);
                    MessageBox.Show("Page Token válido!", "Configuração");
                }
            }
            catch (FacebookOAuthException)
            {
                if (string.IsNullOrEmpty(pageToken.Text))
                    MessageBox.Show("Configure novamente o Page Token, pois encontra-se inválido.", "Erro de Configuração");
                else
                    MessageBox.Show("Configure novamente o App Token e o Page Token, pois encontram-se inválidos.", "Erro de Configuração");
            }
            catch (Exception e)
            {
                if (string.IsNullOrEmpty(appToken.Text) && string.IsNullOrEmpty(pageToken.Text))
                    MessageBox.Show("Configure novamente o App Token e o Page Token, pois encontram-se inválido.\n" + e.Message, "Erro de Configuração");
                else
                    MessageBox.Show("O App Token não possui permissão para requisitar as suas paginas.\n" + e.Message, "Erro de configuração");
            }
        }
    }
}
