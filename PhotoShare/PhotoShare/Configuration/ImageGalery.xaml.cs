﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KinectPhoto.Configuration
{
    /// <summary>
    /// Interaction logic for ImageGalery.xaml
    /// </summary>
    public partial class ImageGalery : UserControl, IPageViewModel
    {

        public string NamePage { get { return name; } }

        public string name { get; set; }
        private List <string> listImage = new List<string>();

        public ImageGalery()
        {
            InitializeComponent();
        }

        public ImageGalery(string name)
        {
            InitializeComponent();
            this.name = name;
            Show();
        }

        public void Show()
        {
            string[] files = Directory.GetFiles(Environment.CurrentDirectory + name);
            if (listImage.Count == files.Length)
                return;

            listImage.Clear();
            galery.Children.Clear();
            listImage.AddRange(files);

            for (int i = 0; i < listImage.Count / 3; i++ )
            {
                RowDefinition row = new RowDefinition();
                row.Height = new GridLength(3, GridUnitType.Star);
                galeryGrid.RowDefinitions.Add(row);
            }

            for (int i = 0; i < listImage.Count; i++)
            {
                string path = listImage[i];
                Image img = new Image();
                img.Margin = new Thickness(3);
                img.MaxWidth = 250;
                img.MaxHeight = 250;

                Uri uriSource = new Uri(path, UriKind.Absolute);
                img.Source = new BitmapImage(uriSource);

                img.SetValue(Grid.RowProperty, i / 3);

                galery.Children.Add(img);
            }
        }

        public void Hide()
        {
        }
    }
}
