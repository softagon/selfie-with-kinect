﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace KinectPhoto.Configuration
{
    /// <summary>
    /// Interaction logic for GeneralConfiguration.xaml
    /// </summary>
    public partial class GeneralConfiguration : UserControl, IPageViewModel
    {
        public string NamePage
        {
            get { return "Configuração Geral"; }
        }

        public GeneralConfiguration()
        {
            InitializeComponent();
        }

        public void Show()
        {
            videoUri.Text = Settings.Get(Settings.Video);
            try
            {
                lipCorner.Value = double.Parse(Settings.Get(Settings.LipCornerDepress));
                lipStretch.Value = double.Parse(Settings.Get(Settings.LipStretch));
            }
            catch(Exception){}
        }

        public void Hide()
        {
        }

        private void Video_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Selecione um video";
            fileDialog.Filter = "All video formats are supported|*.mp4;*.avi;*.wmv";
            fileDialog.Multiselect = false;

            bool? result = fileDialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                videoUri.Text = fileDialog.FileName;
            }

        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Settings.Set(Settings.Video, videoUri.Text);
            Settings.Set(Settings.LipCornerDepress, lipCorner.Text);
            Settings.Set(Settings.LipStretch, lipStretch.Text);
        }


    }
}
