﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using KinectPhoto.Helper;
using Microsoft.Win32;
using System.IO;

namespace KinectPhoto.Configuration
{
    /// <summary>
    /// Interaction logic for IconsConfiguration.xaml
    /// </summary>
    public partial class IconsConfiguration : UserControl, IPageViewModel
    {
        public string NamePage { get { return "Icones"; } }

        private OpenFileDialog fileDialog;

        public IconsConfiguration()
        {
            InitializeComponent();
        }

        public void Show()
        {
            fileDialog = new OpenFileDialog();
            fileDialog.Title = "Selecione uma imagem";
            fileDialog.Filter = "Portable Network Graphic (*.png)|*.png";
            fileDialog.Multiselect = false;

            string parentIconPath = Environment.CurrentDirectory + Directories.DIR_ICONS;
            this.takeIcon.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconTake));
            this.retakeIcon.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconRetake));
            this.shareIcon.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconShare));
            this.printIcon.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconPrint));
            this.configIcon.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.IconConfig));
            parentIconPath = Environment.CurrentDirectory + Directories.DIR_LOGO;
            this.logoIcon.Source = Utils.GetImage(parentIconPath, Settings.Get(Settings.Logo));

            this.logoWidth.Text = Settings.Get(Settings.LogoWidth);
            this.logoHeight.Text = Settings.Get(Settings.LogoHeight);
        }

        public void Hide()
        {
        }

        private ImageSource SelectImage(string internDirectory, string property)
        {
            string parentIconPath = Environment.CurrentDirectory + internDirectory;
            if (fileDialog.ShowDialog() == true)
            {
                try
                {
                    if (!File.Exists(parentIconPath + fileDialog.SafeFileName))
                        File.Copy(fileDialog.FileName, parentIconPath + fileDialog.SafeFileName, true);
                    Settings.Set(property, fileDialog.SafeFileName);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, "Não foi possível trocar o ícone.");
                }
            }
            return Utils.GetImage(parentIconPath, Settings.Get(property));
        }

        private void Take_Click(object sender, RoutedEventArgs r)
        {
            this.takeIcon.Source = SelectImage(Directories.DIR_ICONS, Settings.IconTake);
            MainWindow._iconChanged = Settings.IconTake;
            /*
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.Title = "Selecione uma imagem";
            fileDialog.Filter = "Portable Network Graphic (*.png)|*.png";
            */
            /*if (fileDialog.ShowDialog() == true)
            {
                string parentIconPath = Environment.CurrentDirectory + Directories.DIR_ICONS;

                try{
                    File.Copy(fileDialog.FileName, parentIconPath + fileDialog.SafeFileName);
                    Settings.GetInstance().Set("iconTakePhoto", fileDialog.SafeFileName);
                    this.takeIcon.Source = Utils.GetImage(parentIconPath, Settings.GetInstance().Get("iconTakePhoto"));
                }
                catch(Exception e){
                    MessageBox.Show(e.Message, "Não foi possível trocar o ícone.");
                }
            }*/
        }

        private void Retake_Click(object sender, RoutedEventArgs r)
        {
            this.retakeIcon.Source = SelectImage(Directories.DIR_ICONS, Settings.IconRetake);
            MainWindow._iconChanged = Settings.IconRetake;
        }

        private void Share_Click(object sender, RoutedEventArgs r)
        {
            this.shareIcon.Source = SelectImage(Directories.DIR_ICONS, Settings.IconShare);
            MainWindow._iconChanged = Settings.IconShare;
        }

        private void Print_Click(object sender, RoutedEventArgs r)
        {
            this.printIcon.Source = SelectImage(Directories.DIR_ICONS, Settings.IconPrint);
            MainWindow._iconChanged = Settings.IconPrint;
        }

        private void Config_Click(object sender, RoutedEventArgs r)
        {
            this.configIcon.Source = SelectImage(Directories.DIR_ICONS, Settings.IconConfig);
            MainWindow._iconChanged = Settings.IconConfig;
        }

        private void Logo_Click(object sender, RoutedEventArgs r)
        {
            this.logoIcon.Source = SelectImage(Directories.DIR_LOGO, Settings.Logo);
            MainWindow._iconChanged = Settings.Logo;
        }

        private void Save_Click(object sender, RoutedEventArgs r)
        {
            Settings.Set(Settings.LogoHeight, logoHeight.Text);
            Settings.Set(Settings.LogoWidth, logoWidth.Text);
        }

    }
}
