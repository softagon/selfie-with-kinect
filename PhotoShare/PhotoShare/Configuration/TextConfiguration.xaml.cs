﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KinectPhoto.Configuration
{
    /// <summary>
    /// Interaction logic for TextConfiguration.xaml
    /// </summary>
    public partial class TextConfiguration : UserControl, IPageViewModel
    {
        public string NamePage { get { return "Countdown Text"; } }

        public TextConfiguration()
        {
            InitializeComponent();
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Settings.Set(Settings.CountColor, countColor.SelectedColorText);
            Settings.Set(Settings.CountFontFamily, (string)countFontFamily.SelectedItem);
            Settings.Set(Settings.CountFontSize, countFontSize.Text);

            Settings.Set(Settings.SmileColor, smileColor.SelectedColorText);
            Settings.Set(Settings.SmileFontFamily, (string)smileFontFamily.SelectedItem);
            Settings.Set(Settings.SmileFontSize, smileFontSize.Text);
            Settings.Set(Settings.SmileText, smileText.Text);
            Settings.Set(Settings.NoSmileText, noSmileText.Text);

        }

        public void Show()
        {
            if (countFontFamily.Items.Count <= 0)
            {
                foreach (System.Drawing.FontFamily font in System.Drawing.FontFamily.Families)
                {
                    countFontFamily.Items.Add(font.Name);
                }
            }
            

            if (smileFontFamily.Items.Count <= 0)
            {
                foreach (System.Drawing.FontFamily font in System.Drawing.FontFamily.Families)
                {
                    smileFontFamily.Items.Add(font.Name);
                }
            }

            try
            {
                countFontFamily.SelectedItem = Settings.Get(Settings.CountFontFamily);
                countFontSize.Text = Settings.Get(Settings.CountFontSize);
                countColor.SelectedColor = (Color)ColorConverter.ConvertFromString(Settings.Get(Settings.CountColor));

                smileFontFamily.SelectedItem = Settings.Get(Settings.SmileFontFamily);
                smileFontSize.Text = Settings.Get(Settings.SmileFontSize);
                smileColor.SelectedColor = (Color)ColorConverter.ConvertFromString(Settings.Get(Settings.SmileColor));
                smileText.Text = Settings.Get(Settings.SmileText);
                noSmileText.Text = Settings.Get(Settings.NoSmileText);
            }
            catch (Exception) { }

            
        }

        public void Hide()
        {
        }
    }
}
