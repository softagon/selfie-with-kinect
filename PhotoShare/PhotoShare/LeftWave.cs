﻿using System;
using System.Collections.Generic;
using Microsoft.Kinect;
using KinectPhoto.Helper;

namespace KinectPhoto
{
    class LeftWave : IRecognizerGestures
    {
        public string Name
        {
            get { return "left"; }
        }
        
        public bool Check(Skeleton skeleton)
        {
            Joint elbowRigth = skeleton.Joints[JointType.ElbowRight];
            Joint handRight = skeleton.Joints[JointType.HandRight];
            Joint wristRight = skeleton.Joints[JointType.WristRight];
            if (Utils.Wave(skeleton) &&
                (handRight.Position.X < wristRight.Position.X &&
                 handRight.Position.X < elbowRigth.Position.X &&
                 wristRight.Position.X < elbowRigth.Position.X))
            {
                return true;
            }
            return false;
        }
    }
}
