﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using KinectPhoto;

public sealed class Settings
{
    private readonly string configFile = "Setting.config";
    private readonly XmlDocument xmlDoc = new XmlDocument();
    
    private Uri uriSource;

    public const string IconTake = "iconTakePhoto";
    public const string IconRetake = "iconRetakePhoto";
    public const string IconShare = "iconShare";
    public const string IconPrint = "iconPrint";
    public const string IconConfig = "iconConfig";
    public const string Logo = "logo";
    public const string LogoWidth = "logoWidth";
    public const string LogoHeight = "logoHeight";
    public const string CountColor = "countColor";
    public const string CountFontFamily = "countFontFamily";
    public const string CountFontSize = "countFontSize";
    public const string SmileText = "smileText";
    public const string NoSmileText = "noSmileText";
    public const string SmileColor = "smileColor";
    public const string SmileFontFamily = "smileFontFamily";
    public const string SmileFontSize = "smileFontSize";
    public const string FbPageName = "fbFanPageName";
    public const string FbPageID = "fbFanPageID";
    public const string FbAppToken = "fbAppToken";
    public const string FbAppSecret = "fbAppSecret";
    public const string FbLongToken = "fbLongToken";
    public const string FbMessage = "fbMessage";

    public const string Video = "video";
    public const string LipStretch = "lipStretch";
    public const string LipCornerDepress = "lipCornerDepress";

    private static Settings _instance = new Settings();
    /*public static Settings GetInstance()
    {
        return _instance;
    }*/

    public static string Get(string key)
    {
        return _instance.GetValue(key);
    }

    public static void Set(string key, string value)
    {
        _instance.SetValue(key, value);
    }

    private Settings()
    {
        uriSource = new Uri(Environment.CurrentDirectory + Directories.DIR_CONFIG + configFile, UriKind.Absolute);
        xmlDoc.Load(uriSource.ToString());
    }

    private string GetValue(string key)
    {
        XmlNode node = xmlDoc.SelectSingleNode("configuration/appSettings/add[@key='" + key + "']");

        if (node == null)
        {
            return "";
        }
        return node.Attributes["value"].Value ?? "";
    }

    private void SetValue(string key, string value)
    {
        XmlNode node = xmlDoc.SelectSingleNode("configuration/appSettings/add[@key='" + key + "']");

        if (node != null && uriSource != null)
        {
            node.Attributes["value"].Value = value;
            xmlDoc.Save(uriSource.OriginalString);
        }
    }

}