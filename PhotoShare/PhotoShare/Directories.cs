﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinectPhoto
{
    class Directories
    {

        public const string DIR_UPLOADED_PHOTOS = "\\Images\\Photos\\Uploaded\\";
        public const string DIR_NO_UPLOADED_PHOTOS = "\\Images\\Photos\\NoUploaded\\";
        public const string DIR_ICONS = "\\Images\\Icons\\";
        public const string DIR_LOGO = "\\Images\\Logo\\";
        public const string DIR_CONFIG = "\\Config\\";
    }
}
